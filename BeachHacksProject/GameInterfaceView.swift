//
//  GameInterfaceView.swift
//  BeachHacksProject
//
//  Created by Andres Arciniegas on 4/2/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import UIKit

class GameInterfaceView: UIView {
    var heart1View: UIImageView?
    var heart2View: UIImageView?
    var heart3View: UIImageView?
    var heart4View: UIImageView?
    var magnumView: UIImageView?
    var shootButton: UIButton?
    var changeGunButton: UIButton?
    var image : UIImage = UIImage(named:"heart")!
    var image2 : UIImage = UIImage(named:"magnum")!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.clearColor()
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func setNumberOfHearts(count: Int) {
        var hearts = [heart1View, heart2View, heart3View, heart4View]
        
        for i in 0...3 {
            hearts[i]?.hidden = count <= i
        }
    }
    
    func setup () {
        heart1View = UIImageView(image: image)
        heart1View!.backgroundColor = UIColor.clearColor()
        heart1View!.alpha = 0.6
        heart1View!.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(heart1View!, atIndex: 10)
        
        heart2View = UIImageView(image: image)
        heart2View!.backgroundColor = UIColor.clearColor()
        heart2View!.alpha = 0.6
        heart2View!.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(heart2View!, atIndex: 10)
        
        heart3View = UIImageView(image: image)
        heart3View!.backgroundColor = UIColor.clearColor()
        heart3View!.alpha = 0.6
        heart3View!.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(heart3View!, atIndex: 10)
        
        heart4View = UIImageView(image: image)
        heart4View!.backgroundColor = UIColor.clearColor()
        heart4View!.alpha = 0.6
        heart4View!.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(heart4View!, atIndex: 10)
        
        magnumView = UIImageView(image: image2)
        magnumView!.backgroundColor = UIColor.clearColor()
        magnumView!.alpha = 1.0
        magnumView!.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(magnumView!, atIndex: 10)
        
        shootButton = UIButton()
        shootButton!.backgroundColor = UIColor(red: 43/255, green: 135/255.0, blue: 200/255.0, alpha: 0.6)
        shootButton!.layer.cornerRadius = 55
        shootButton!.layer.borderWidth = 0.5
        shootButton!.layer.borderColor = UIColor(red: 196/255, green: 232/255.0, blue: 245/255.0, alpha: 0.8).CGColor
        shootButton!.setTitle("SHOOT!", forState: UIControlState.Normal)
        shootButton!.titleLabel!.font =  UIFont(name: "Arial Rounded MT Bold", size: 20.0)
        shootButton!.setTitleColor(UIColor(red: 196/255, green: 232/255.0, blue: 245/255.0, alpha: 0.8), forState: UIControlState.Normal)
        shootButton!.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(shootButton!, atIndex: 10)
        
        changeGunButton = UIButton()
        changeGunButton!.backgroundColor = UIColor.greenColor()
        changeGunButton!.layer.cornerRadius = 55
        changeGunButton!.layer.borderWidth = 0.5
        changeGunButton!.layer.borderColor = UIColor(red: 119/255, green: 167/255.0, blue: 36/255.0, alpha: 0.8).CGColor
        changeGunButton!.setTitle("SNIPER", forState: UIControlState.Normal)
        changeGunButton!.titleLabel!.font =  UIFont(name: "Arial Rounded MT Bold", size: 20.0)
        changeGunButton!.setTitleColor(UIColor(red: 196/255, green: 232/255.0, blue: 245/255.0, alpha: 0.8), forState: UIControlState.Normal)
        changeGunButton!.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(changeGunButton!, atIndex: 10)
        
        let views = [
            "shootButton": shootButton!,
            "changeGunButton": changeGunButton!,
            "heart1View": heart1View!,
            "heart2View": heart2View!,
            "heart3View": heart3View!,
            "heart4View": heart4View!,
            "magnumView": magnumView!,
        ]
        
        let shootButtonConstraints = ["V:[shootButton(110)]-30-|", "H:[shootButton(110)]-30-|"]
        self.visualFormatConstraints(shootButtonConstraints, views: views)
        
        let changeGunButtonConstraints = ["V:[changeGunButton(110)]-30-[shootButton]", "H:[changeGunButton(110)]-30-|"]
        self.visualFormatConstraints(changeGunButtonConstraints, views: views)
        
        let heart1ViewConstraints = ["V:[heart1View(50)]-15-|", "H:|-15-[heart1View(50)]"]
        visualFormatConstraints(heart1ViewConstraints, views: views)
        
        let heart2ViewConstraints = ["V:[heart2View(50)]-15-|", "H:[heart1View]-15-[heart2View(50)]"]
        visualFormatConstraints(heart2ViewConstraints, views: views)
        
        let heart3ViewConstraints = ["V:[heart3View(50)]-15-|", "H:[heart2View]-15-[heart3View(50)]"]
        visualFormatConstraints(heart3ViewConstraints, views: views)
        
        let heart4ViewConstraints = ["V:[heart4View(50)]-15-|", "H:[heart3View]-15-[heart4View(50)]"]
        visualFormatConstraints(heart4ViewConstraints, views: views)
        
        let magnumViewConstraints = ["V:[magnumView(150)]-0-|", "H:[heart4View]-15-[magnumView(150)]"]
        visualFormatConstraints(magnumViewConstraints, views: views)
    }
    
}