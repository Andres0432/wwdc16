//
//  GameInterfaceViewController.swift
//  BeachHacksProject
//
//  Created by Andres Arciniegas on 4/2/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import UIKit

enum GameInterfaceControllerEventId {
    case DID_SHOOT
    case DID_CHANGE_WEAPON
}

class GameInterfaceViewController: UIViewController, KFEventEmmiter {
    let mainView = GameInterfaceView()
    var haloLayer: CAShapeLayer?
    var muzzleFlashLayer: CAShapeLayer?
    var muzzleFlashLayer2: CAShapeLayer?
    var muzzleFlashLayers = [CAShapeLayer]()
    var crossHairLayers = [CAShapeLayer]()
    
    var eventHandler: KFEventHandler!
    
    func setHandler(handler: KFEventHandler) {
        eventHandler = handler
    }
    
    override func loadView() {
        self.view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        self.mainView.shootButton!.addTarget(self, action: "shootButtonPressed", forControlEvents: UIControlEvents.TouchUpInside)
        
    }
    
    func shootButtonPressed() {
        let id = GameInterfaceControllerEventId.DID_SHOOT as Any
        let shootEvent = KFEvent(source: "GameInterfaceController", identifier: id)
        
        eventHandler.processEvent(shootEvent)
    }
    
    func setHeartCount(count: Int) {
        self.mainView.setNumberOfHearts(count)
    }
    
    func animateGettingHit() {
        
    }
    
    func setup() {
        let pi = M_PI
        let radius = CGFloat(20.0)
        let archPath = UIBezierPath()
        
        haloLayer = CAShapeLayer()
        haloLayer!.fillColor = UIColor.clearColor().CGColor
        haloLayer!.strokeColor = UIColor.blackColor().CGColor
        haloLayer!.contentsScale = UIScreen.mainScreen().scale
        
        let startAngle = CGFloat(0.0 * pi)
        let endAngle = CGFloat(-2.0 * pi)
        let tangent = CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + 20.0, UIScreen.mainScreen().bounds.size.height/2.0)
        let center = CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0, UIScreen.mainScreen().bounds.size.height/2.0)
        
        archPath.moveToPoint(tangent)
        archPath.addArcWithCenter(center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: false)
        archPath.closePath()
        
        haloLayer!.path = archPath.CGPath
        haloLayer!.lineWidth = CGFloat(5.0)
        self.mainView.layer.addSublayer(haloLayer!)
        crossHairLayers.append(haloLayer!)
        
        for index in 0...3 {
            let crossHairShapeLayer = CAShapeLayer()
            let crossHairPath = UIBezierPath()
            crossHairPath.lineJoinStyle = CGLineJoin.Round
            
            crossHairShapeLayer.fillColor = UIColor.blackColor().CGColor
            crossHairShapeLayer.strokeColor = UIColor.blackColor().CGColor
            crossHairShapeLayer.contentsScale = UIScreen.mainScreen().scale
            
            switch index {
            case 0:
                crossHairPath.moveToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (10.0), UIScreen.mainScreen().bounds.size.height/2.0 + (0.0)))
                crossHairPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (30.0), UIScreen.mainScreen().bounds.size.height/2.0 + (0.0)))
                crossHairPath.closePath()
            case 1:
                crossHairPath.moveToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (0.0), UIScreen.mainScreen().bounds.size.height/2.0 + (10.0)))
                crossHairPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (0.0), UIScreen.mainScreen().bounds.size.height/2.0 + (30.0)))
                crossHairPath.closePath()
            case 2:
                crossHairPath.moveToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-10.0), UIScreen.mainScreen().bounds.size.height/2.0 + (0.0)))
                crossHairPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-30.0), UIScreen.mainScreen().bounds.size.height/2.0 + (0.0)))
                crossHairPath.closePath()
            case 3:
                crossHairPath.moveToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (0.0), UIScreen.mainScreen().bounds.size.height/2.0 + (-10.0)))
                crossHairPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (0.0), UIScreen.mainScreen().bounds.size.height/2.0 + (-30.0)))
                crossHairPath.closePath()
                
            default:
                break
            }
            
            crossHairPath.lineCapStyle = CGLineCap.Round
            crossHairPath.lineJoinStyle = CGLineJoin.Round
            crossHairShapeLayer.path = crossHairPath.CGPath
            crossHairShapeLayer.lineWidth = CGFloat(5.0)
            self.mainView.layer.addSublayer(crossHairShapeLayer)
            
            crossHairLayers.append(crossHairShapeLayer)
        }
        
        muzzleFlashLayer = CAShapeLayer()
        muzzleFlashLayer!.fillColor = UIColor.clearColor().CGColor
        muzzleFlashLayer!.strokeColor = UIColor.clearColor().CGColor
        muzzleFlashLayer!.contentsScale = UIScreen.mainScreen().scale
        muzzleFlashLayers.append(muzzleFlashLayer!)
        
        muzzleFlashLayer2 = CAShapeLayer()
        muzzleFlashLayer2!.fillColor = UIColor.clearColor().CGColor
        muzzleFlashLayer2!.strokeColor = UIColor.clearColor().CGColor
        muzzleFlashLayer2!.contentsScale = UIScreen.mainScreen().scale
        muzzleFlashLayers.append(muzzleFlashLayer2!)
        
        var ratio = CGFloat(0.1)
        
        for (index, element) in muzzleFlashLayers.enumerate() {
            if index == 1 {
                ratio = CGFloat(0.05)
            }
            
            let tangent = CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (21.43 * ratio), UIScreen.mainScreen().bounds.size.height/2.0)
            let muzzleFlashPath = UIBezierPath()
            
            muzzleFlashPath.moveToPoint(tangent)
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (38.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (15.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (14.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (11.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (20.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (29.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (7.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (19.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (5.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (44.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-6.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (18.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-18.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (31.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-18.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (13.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-40.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (21.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-24.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (6.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-36.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (0.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-21.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-6.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-39.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-16.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-18.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-15.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-17.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-31.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-8.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-16.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (2.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-26.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (5.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-18.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (19.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-28.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (16.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-16.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (38.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-27.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (24.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-13.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (40.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-3.0 * ratio)))
            muzzleFlashPath.closePath()
            
            element.path = muzzleFlashPath.CGPath
            self.mainView.layer.addSublayer(element)
        }
    }
    
    func muzzleFlashAnimation() {
        var ratio = CGFloat(2.0)
        
        for (index, element) in muzzleFlashLayers.enumerate() {
            if index == 1 {
                ratio = CGFloat(1.5)
            }
            
            let tangent = CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (21.43 * ratio), UIScreen.mainScreen().bounds.size.height/2.0)
            let muzzleFlashPath = UIBezierPath()
            
            muzzleFlashPath.moveToPoint(tangent)
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (38.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (15.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (14.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (11.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (20.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (29.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (7.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (19.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (5.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (44.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-6.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (18.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-18.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (31.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-18.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (13.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-40.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (21.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-24.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (6.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-36.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (0.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-21.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-6.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-39.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-16.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-18.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-15.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-17.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-31.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (-8.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-16.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (2.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-26.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (5.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-18.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (19.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-28.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (16.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-16.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (38.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-27.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (24.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-13.0 * ratio)))
            muzzleFlashPath.addLineToPoint(CGPointMake(UIScreen.mainScreen().bounds.size.width/2.0 + (40.0 * ratio), UIScreen.mainScreen().bounds.size.height/2.0 + (-3.0 * ratio)))
            muzzleFlashPath.closePath()
            
            let animationDuration = CFTimeInterval(0.1)
            let animationBeginTime = CACurrentMediaTime()
            
            let animation = CABasicAnimation(keyPath: "path")
            animation.toValue = muzzleFlashPath.CGPath
            animation.removedOnCompletion = true
            animation.fillMode = kCAFillModeForwards
            animation.beginTime = CFTimeInterval(animationBeginTime)
            animation.autoreverses = false
            animation.duration = animationDuration
            element.addAnimation(animation, forKey: "path")
            
            let animation2Duration = CFTimeInterval(0.1)
            
            let animation2 = CABasicAnimation(keyPath: "fillColor")
            
            if index == 0 {
                animation2.toValue = UIColor.blackColor().CGColor
            } else {
                animation2.toValue = UIColor(red: 250/255, green: 193/255.0, blue: 0/255.0, alpha: 0.8).CGColor
            }
            
            animation2.removedOnCompletion = true
            animation2.fillMode = kCAFillModeForwards
            animation2.beginTime = CFTimeInterval(animationBeginTime)
            animation2.autoreverses = false
            animation2.duration = animation2Duration
            element.addAnimation(animation2, forKey: "fillColor")
        }
    }
    
    func animateCrossHair() {
        print("animateCrossHair")
//        
//        for (index, element) in crossHairLayers.enumerate() {
//            
//            let animationDuration = CFTimeInterval(0.1)
//            let animationBeginTime = CACurrentMediaTime()
//            
//            let animation = CABasicAnimation(keyPath: "strokeColor")
//            animation.toValue = UIColor.whiteColor().CGColor
//            animation.removedOnCompletion = false
//            animation.fillMode = kCAFillModeForwards
//            animation.beginTime = CFTimeInterval(animationBeginTime)
//            animation.autoreverses = false
//            animation.duration = animationDuration
//            animation.repeatCount = 10
//            element.addAnimation(animation, forKey: "strokeColor")
//        }
    }
}

