//
//  VideoProcessingViewController.m
//  OpenCVSampleApp-iOS
//
//  Created by Кирилл on 3/31/16.
//  Copyright © 2016 BrainDump. All rights reserved.
//

#import "VideoProcessingViewController.h"

#include "CSimpleTargetDetector.hpp"

CSimpleTargetDetector targetDetector(false);

@implementation VideoProcessingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationLandscapeRight];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    _videoCamera = [[CvVideoCamera alloc] initWithParentView: _imageView];
    _videoCamera.delegate = self;
    _videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    _videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationLandscapeRight;
    _videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    _videoCamera.defaultFPS = 30;
    _videoCamera.grayscaleMode = NO;
    _videoCamera.rotateVideo = NO;
    
    [_videoCamera start];
    
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setDelegate: (UIViewController*) logicViewController {
    NSLog(@"*** VideoProcessingViewController::setDelegate\n");
    
    _callbackInterface = [[MainAppCallbacksInterface alloc] init];
    
    [_callbackInterface setDel: logicViewController];
}

#ifdef __cplusplus
- (void) processImage:(cv::Mat&)image {
    int centerX = image.size().width / 2;
    int canterY = image.size().height / 2;
    
    cv::Mat subImage = image(cv::Rect(centerX - 100, canterY - 100, 200, 200));
    
    targetDetector.processFrame(subImage);
    targetDetector.detectRedCircles();
    std::string detectedTarget = targetDetector.detectedTarget();
    targetDetector.highlightFrame();
    
    NSString* targetShapeObjcStr = [[NSString alloc] initWithCString: detectedTarget.c_str()
                                                            encoding: [NSString defaultCStringEncoding]];
    [_callbackInterface targetDetected: targetShapeObjcStr targetColor: @"RED"];
}
#endif

@end
