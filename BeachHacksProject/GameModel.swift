//
//  GameModel.swift
//  BeachHacksProject
//
//  Created by Andres Arciniegas on 4/2/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import GameKit

enum GameEventId {
    case TARGET_HIT
    case PLAYER_GOT_HIT
    case PLAYER_DID_SHOOT
    case PLAYER_KILLED
    case PLAYER_WON
}

enum WeaponType: Int {
    case SNIPER         = 4
    case SEMI_AUTOMATIC = 1
}

final class GameModel: NSObject, KFEventEmmiter, GKMatchDelegate {
    let gameCenterMatch: GKMatch
    var eventProcessor:  KFEventHandler!
    
    var gameActive: Bool
    var targetCurrentlyInScope = false
    
    var localPlayerHealth: Int
    
    var activeWeapon: WeaponType
    var timer: NSTimer!
    var weaponLoaded: Bool {
        didSet {
            print("weaponLoaded", weaponLoaded)
            
            if weaponLoaded == false {
                var loadingTime: Double = 1
                if activeWeapon == .SNIPER {
                    loadingTime = 4
                }
                
                timer = NSTimer.scheduledTimerWithTimeInterval(loadingTime, target: self, selector: "loaded", userInfo: nil, repeats: false)
            }
        }
    }
    
    func loaded() {
        print("loaded")
        dispatch_async(dispatch_get_main_queue()) {
            () -> Void in
            self.weaponLoaded = true
        }
    }
    
    // MARK: - Model setup
    init(match: GKMatch) {
        activeWeapon = WeaponType.SEMI_AUTOMATIC
        weaponLoaded = true
        localPlayerHealth = 4
        
        gameActive = false
        gameCenterMatch = match
        
        super.init()
        
        gameCenterMatch.delegate = self
    }
    
    func setHandler(handler: KFEventHandler) {
        self.eventProcessor = handler
    }
    
    // MARK: - Get info from model
    func getCurrentWeaponType() -> WeaponType {
        return activeWeapon
    }
    
    // MARK: - Model update methods
    func reset() {
        activeWeapon = WeaponType.SEMI_AUTOMATIC
        weaponLoaded = true

        localPlayerHealth = 4
        
        gameActive = false
    }
    
    func start() {
        gameActive = true
    }
    
    // Detection updates
    func targetAppearedInScope() {
        targetCurrentlyInScope = true
    }
    
    func allTargetsOutOfScope() {
        targetCurrentlyInScope = false
    }
    
    // Logic view controller updates
    func playerDidTapTrigger() {
        print("playerDidTapTrigger")
        if weaponLoaded == false {
            print("weapon not loaded")
            return
        }
        
        let shootId = GameEventId.PLAYER_DID_SHOOT as AnyEnum
        let shootEvent = KFEvent(source: "GameModel", identifier: shootId)
        eventProcessor.processEvent(shootEvent)
        
        weaponLoaded = false
        
        if targetCurrentlyInScope == false {
            print("target not in scope")
            return
        }
        
        let hitTargetEventId = GameEventId.TARGET_HIT as AnyEnum
        let targetHitEvent = KFEvent(source: "GameModel", identifier: hitTargetEventId, data: targetCurrentlyInScope)
        eventProcessor.processEvent(targetHitEvent)
        
        var score: NSInteger = activeWeapon.rawValue
        let data = NSData(bytes: &score, length: sizeof(NSInteger))
        do {
            try gameCenterMatch.sendDataToAllPlayers(data, withDataMode: GKMatchSendDataMode.Reliable)
        } catch {
            print(error)
        }
    }
    
    func changeWeapon(newWeapon: WeaponType) {
        print("changeWeapon")
        activeWeapon = newWeapon
    }
    
    // MARK: - GKMatchDelegate
    func match(match: GKMatch, didReceiveData data: NSData, fromRemotePlayer player: GKPlayer) {
        if gameActive != true {
            return
        }
        
        var hitStrength: NSInteger = 0
        data.getBytes(&hitStrength, length: sizeof(NSInteger));
        
        if hitStrength == -1 {
            let id = GameEventId.PLAYER_WON as AnyEnum
            let playerWonEvent = KFEvent(source: "GameModel", identifier: id)
            eventProcessor.processEvent(playerWonEvent)
            return
        }
        
        localPlayerHealth -= Int(hitStrength)
        
        if localPlayerHealth > 0 {
            let id = GameEventId.PLAYER_GOT_HIT as AnyEnum
            let playerGotHitEvent = KFEvent(source: "GameModel", identifier: id, data: localPlayerHealth as AnyObject)
            eventProcessor.processEvent(playerGotHitEvent)
            
            return
        }
        
        let id = GameEventId.PLAYER_KILLED as AnyEnum
        let playerKilledEvent = KFEvent(source: "GameModel", identifier: id)
        eventProcessor.processEvent(playerKilledEvent)
        
        var score: NSInteger = -1
        let data = NSData(bytes: &score, length: sizeof(NSInteger))
        do {
            try gameCenterMatch.sendDataToAllPlayers(data, withDataMode: GKMatchSendDataMode.Reliable)
        } catch {
            print(error)
        }
    }
}


