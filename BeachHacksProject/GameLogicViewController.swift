//
//  ViewController.swift
//  BeachHacksProject
//
//  Created by Andres Arciniegas on 4/2/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import UIKit

class GameLogicViewController: UIViewController, KFEventHandler {
    let mainView = GameLogicView()
    
    let videoProcessingController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("VideoProcessingViewController")
    let gameInterfaceController = GameInterfaceViewController()
    
    let gameModel: GameModel
    
    // MARK: - Setup view controller
    override func loadView() {
        self.view = mainView
        
//        let button = UIButton()
//        button.backgroundColor = UIColor.clearColor()
//        button.setTitle("ChangeWeapon", forState: .Normal)
//        button.setTitleColor(UIColor.grayColor(), forState: .Normal)
        
//        button.addTarget(self, action: "changeWeapon", forControlEvents: UIControlEvents.TouchUpInside)
//        
//        self.gameInterfaceController.mainView.addSubview(button)
//        button.translatesAutoresizingMaskIntoConstraints = false
//        let views = ["button": button]
//        let buttonConstraints = ["V:|-50-[button(100)]", "H:|-50-[button(100)]"]
//        self.gameInterfaceController.mainView.visualFormatConstraints(buttonConstraints, views: views)
    }
    
    func changeWeapon() {
        let id = GameInterfaceControllerEventId.DID_CHANGE_WEAPON as AnyEnum
        let changeWeaponEvent = KFEvent(source: "GameInterfaceController", identifier: id)
        
        self.processEvent(changeWeaponEvent)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gameModel.setHandler(self)
        gameModel.start()
        
        gameInterfaceController.setHandler(self)
        
        addVideoProcessingViewController()
        addInterfaceViewController()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        gameModel.start()
    }

    init(gameModel: GameModel) {
        self.gameModel = gameModel
        
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addVideoProcessingViewController() {
        videoProcessingController.performSelector("setDelegate:", withObject: self)
        
        let views = ["videoProcessingControllerView": videoProcessingController.view]
        
        self.addChildViewController(videoProcessingController)
        self.mainView.addSubview(videoProcessingController.view)
        videoProcessingController.view.translatesAutoresizingMaskIntoConstraints = false
        
        let videoProcessingControllerViewConstraints = [
            "V:|-0-[videoProcessingControllerView]-0-|",
            "H:|-0-[videoProcessingControllerView]-0-|"
        ]
        self.mainView.visualFormatConstraints(videoProcessingControllerViewConstraints, views: views)
        
        videoProcessingController.didMoveToParentViewController(self)
    }
    
    func addInterfaceViewController() {
        let views = ["gameInterfaceControllerMainView": gameInterfaceController.view]
        
        self.addChildViewController(gameInterfaceController)
        self.mainView.addSubview(gameInterfaceController.view)
        gameInterfaceController.mainView.translatesAutoresizingMaskIntoConstraints = false
        
        let gameInterfaceControllerMainViewConstraints = [
            "V:|-0-[gameInterfaceControllerMainView]-0-|",
            "H:|-0-[gameInterfaceControllerMainView]-0-|"
        ]
        self.mainView.visualFormatConstraints(gameInterfaceControllerMainViewConstraints, views: views)
        
        gameInterfaceController.didMoveToParentViewController(self)
    }
    
    // MARK: - Process events
    func processObjectDetectorControllerEvent(objectShape: String, color: String) {
        if objectShape == "EMPTY" {
            gameModel.allTargetsOutOfScope()
        } else {
            gameModel.targetAppearedInScope()
            
            gameInterfaceController.animateCrossHair()
        }
    }
    
    func processEvent(event: KFEvent) {
        print(event)
        
        switch event.source {
        case "GameModel":
            switch event.identifier as! GameEventId {
            case .PLAYER_DID_SHOOT:
                print("PLAYER_DID_SHOOT")
                
                gameInterfaceController.muzzleFlashAnimation()
            case .TARGET_HIT:
                print("TARGET_HIT")
                
            
            case .PLAYER_GOT_HIT:
                print("PLAYER_GOT_HIT")
                
                let livesLeft = event.data as! Int
                
                print("livesLeft", livesLeft)
                gameInterfaceController.setHeartCount(livesLeft)
            case .PLAYER_KILLED:
                print("PLAYER_KILLED")
                
                let gameOverViewController = GameOverViewController(message: "GAME OVER. YOU LOOSE!")
                self.presentViewController(gameOverViewController, animated: true, completion: {
                    () -> Void in
                    self.gameModel.reset()
                    self.gameInterfaceController.setHeartCount(4)
                })
            case .PLAYER_WON:
                print("PLAYER_WON")
                
                let gameOverViewController = GameOverViewController(message: "GAME OVER. YOU WIN!")
                self.presentViewController(gameOverViewController, animated: true, completion: {
                    () -> Void in
                    self.gameModel.reset()
                    self.gameInterfaceController.setHeartCount(4)
                })
            }
        case "GameInterfaceController":
            switch event.identifier as! GameInterfaceControllerEventId {
            case .DID_SHOOT:
                print("Did Shoot")
                
                gameModel.playerDidTapTrigger()
            case .DID_CHANGE_WEAPON:
                print("DID_CHANGE_WEAPON")
                
                let weaponType = gameModel.getCurrentWeaponType()
                if weaponType == .SEMI_AUTOMATIC {
                    gameModel.changeWeapon(.SNIPER)
                } else {
                    gameModel.changeWeapon(.SEMI_AUTOMATIC)
                }
            }
        default:
            fatalError("*** processEvent: unknown event source");
        }
    
    }
}

