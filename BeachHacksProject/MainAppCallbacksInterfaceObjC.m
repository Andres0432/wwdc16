//
//  MainAppCallbacksInterfaceObjC.m
//  DarkWire
//
//  Created by Кирилл on 3/5/16.
//  Copyright © 2016 BrainDump. All rights reserved.
//

#import <BeachHacksProject-Swift.h>

#import <Foundation/Foundation.h>
#import "MainAppCallbacksInterfaceObjC.h"

@implementation MainAppCallbacksInterface

- (void) setDel: (UIViewController*) delegate {
    _delegate = (GameLogicViewController*)delegate;
}

- (void) targetDetected: (NSString*) objectShape
            targetColor: (NSString*) color {
    [_delegate processObjectDetectorControllerEvent: objectShape color:color];
}

@end

