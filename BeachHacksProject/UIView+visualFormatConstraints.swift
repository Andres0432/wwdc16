//
//  UIView+visualFormatConstraints.swift
//  BeachHacksProject
//
//  Created by Andres Arciniegas on 4/2/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func visualFormatConstraints(visualFormatConstraints: [String], views: Dictionary<String, AnyObject>) {
        var constraints = [NSLayoutConstraint]()
        
        for visualConstraint in visualFormatConstraints {
            let constraintsGroup = NSLayoutConstraint.constraintsWithVisualFormat(visualConstraint, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
            constraints.appendContentsOf(constraintsGroup)
        }
        
        self.addConstraints(constraints)
    }
}