//
//  GameCenterMatchViewController.swift
//  BeachHacksProject
//
//  Created by Кирилл on 4/3/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import GameKit

class GameCenterMatchViewController: GKMatchmakerViewController, GKMatchmakerViewControllerDelegate, UINavigationControllerDelegate {
    
    override init?(matchRequest request: GKMatchRequest) {
        super.init(matchRequest: request)
        
        self.matchmakerDelegate = self
    }
    
    override init?(invite: GKInvite) {
        super.init(invite: invite)
        
        self.matchmakerDelegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    required override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    func matchmakerViewController(viewController: GKMatchmakerViewController, didFindMatch match: GKMatch) {
        print("didFindMatch")
        
        let gameModel = GameModel(match: match)
        let gameLogicViewController = GameLogicViewController(gameModel: gameModel)
        
        self.presentViewController(gameLogicViewController, animated: true, completion: nil)
    }
    
    func matchmakerViewControllerWasCancelled(viewController: GKMatchmakerViewController) {
        fatalError("matchmakerViewControllerWasCancelled")
    }
    
    func matchmakerViewController(viewController: GKMatchmakerViewController, didFailWithError error: NSError) {
        fatalError("matchmakerViewControllerWasCancelled")
    }
    
    func player(player: GKPlayer, didRequestMatchWithRecipients recipientPlayers: [GKPlayer]) {
        print("didRequestMatchWithRecipients")
        print(recipientPlayers.first?.playerID)
    }
}
