//
//  MainAppCallbacksInterfaceObjC.h
//  DarkWire
//
//  Created by Кирилл on 3/5/16.
//  Copyright © 2016 BrainDump. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef MainAppCallbacksInterfaceObjC_h
#define MainAppCallbacksInterfaceObjC_h

@class GameLogicViewController;

@interface MainAppCallbacksInterface: NSObject

@property (nonatomic, retain) GameLogicViewController* delegate;

- (void) setDel: (UIViewController*) delegate;

- (void) targetDetected: (NSString*) objectShape
            targetColor: (NSString*) color;

@end


#endif /* MainAppCallbacksInterfaceObjC_h */
