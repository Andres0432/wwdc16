//
//  File.swift
//  BeachHacksProject
//
//  Created by Andres Arciniegas on 4/2/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation

typealias AnyEnum = Any

class KFEvent {
    var source:     String
    var identifier: AnyEnum
    
    var description: String?
    var data:        Any?
    
    init(source: String, identifier: AnyEnum, data: Any?) {
        self.source     = source
        self.identifier = identifier
        
        self.data = data
    }
    
    convenience init(source: String, identifier: AnyEnum) {
        self.init(source: source, identifier: identifier, data: nil)
    }
}

protocol KFEventHandler {
    func processEvent(event: KFEvent)
}

protocol KFEventEmmiter {
    func setHandler(_: KFEventHandler)
}