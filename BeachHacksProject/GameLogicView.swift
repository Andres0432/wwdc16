//
//  GameLogicView.swift
//  BeachHacksProject
//
//  Created by Andres Arciniegas on 4/2/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import UIKit

class GameLogicView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.greenColor()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
}