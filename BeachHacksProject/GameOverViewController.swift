//
//  GameOverViewController.swift
//  BeachHacksProject
//
//  Created by Кирилл on 4/3/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import UIKit

class GameOverViewController: UIViewController {
    let messageLabel = UILabel()
    
    var timer: NSTimer!
    
    init(message: String) {
        super.init(nibName: nil, bundle: nil)
        
        messageLabel.text = message
        view.backgroundColor = UIColor.whiteColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(messageLabel)
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // Center horizontally
        var constraints = NSLayoutConstraint.constraintsWithVisualFormat(
            "V:[superview]-(<=1)-[label]",
            options: NSLayoutFormatOptions.AlignAllCenterX,
            metrics: nil,
            views: ["superview": view, "label": messageLabel])
        
        view.addConstraints(constraints)
        
        // Center vertically
        constraints = NSLayoutConstraint.constraintsWithVisualFormat(
            "H:[superview]-(<=1)-[label]",
            options: NSLayoutFormatOptions.AlignAllCenterY,
            metrics: nil,
            views: ["superview": view, "label": messageLabel])
        
        view.addConstraints(constraints)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: "dismissViewController", userInfo: nil, repeats: false)
    }
    
    func dismissViewController() {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
}


