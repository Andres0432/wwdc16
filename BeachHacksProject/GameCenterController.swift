//
//  GameCenterController.swift
//  BeachHacksProject
//
//  Created by Кирилл on 4/3/16.
//  Copyright © 2016 Andres Arciniegas. All rights reserved.
//

import Foundation
import GameKit

class GameCenterController: UIViewController, GKGameCenterControllerDelegate, GKLocalPlayerListener {
    var gameCenterEnabled = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Your code that sets up your scene or other set up code
        
        let button = UIButton()
        button.backgroundColor = UIColor.clearColor()
        button.setTitle("Invite", forState: .Normal)
        button.setTitleColor(UIColor.blackColor(), forState: .Normal)
        
        button.addTarget(self, action: "loadInviteScreen", forControlEvents: UIControlEvents.TouchUpInside)
        
        self.view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        let views = ["button": button]
        let buttonConstraints = ["V:|-50-[button(70)]", "H:|-50-[button(70)]"]
        self.view.visualFormatConstraints(buttonConstraints, views: views)
        
        //HERE IS WHERE YOU AUTHENTICATE
        authenticateLocalPlayer()
    }

    func authenticateLocalPlayer() {
        print("authenticateLocalPlayer")
        
        let localPlayer = GKLocalPlayer.localPlayer()
        localPlayer.authenticateHandler = {
            (viewController: UIViewController?, error : NSError?) -> Void in
                if viewController != nil {
                    print("authenticateHandler; viewController != nil")

                    self.presentViewController(viewController!, animated:true, completion: nil)
                } else {
                    print("authenticateHandler; viewController == nil")
                    
                    if localPlayer.authenticated {
                        print("localPlayer is authenticated")
                        
                        self.gameCenterEnabled = true
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("AUTHENTICATED_NOTIFICATION", object: nil)
                        localPlayer.registerListener(self)
                        
                    } else {
                        print("not able to authenticate fail")
                        self.gameCenterEnabled = false
                        
                        if error != nil {
                            print("\(error!.description)")
                        } else {
                            print("error is nil")
                        }
                    }
                }
        }
    }

    func gameCenterViewControllerDidFinish(gameCenterViewController: GKGameCenterViewController) {
        print("gameCenterViewControllerDidFinish")
        
        gameCenterViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func player(player: GKPlayer, didAcceptInvite invite: GKInvite) {
        print("didAcceptInvite")
        
        let matchMackerController = GameCenterMatchViewController(invite: invite)
        self.presentViewController(matchMackerController!, animated: true, completion: nil)
    }
    
    func player(player: GKPlayer, didRequestMatchWithRecipients recipientPlayers: [GKPlayer]) {
        print("didRequestMatchWithRecipients")
        print(recipientPlayers.first?.playerID)
    }
    
    func loadInviteScreen() {
        
        let matchRequest = GKMatchRequest()
        matchRequest.minPlayers = 2
        matchRequest.maxPlayers = 2
        
        matchRequest.inviteMessage = "Lets play a game, Yo"
        
        let matchMackerController = GameCenterMatchViewController(matchRequest: matchRequest)
        self.presentViewController(matchMackerController!, animated: true, completion: nil)
    }
}
